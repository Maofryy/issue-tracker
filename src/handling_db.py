from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import backref
import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///issueTracker.db'
db = SQLAlchemy(app)

# Table for Many to Many relationships
association_table = db.Table('association', db.Base.metadata,
    db.Column('left_id', db.Integer, db.ForeignKey('left.id')),
    db.Column('right_id', db.Integer, db.ForeignKey('right.id'))
)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    role = db.Column(db.String(80), nullable=True)
    #Many to Many
    projects = db.Column(db.Strings(120), nullable=True)
    #One to Many
    tickets = db.Column(db.Strings(120), nullable=True)

    def __repr__(self):
        return '<User %r %r>' % (self.username, self.email)

class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.String(240), nullable=True)
    #One to Many
    tickets = db.relationship("Ticket", back_populates="project")
    #Many to Many
    users = db.Column(db.Strings(120), nullable=True)

    def __repr__(self):
        return '<Project %r>' % (self.name)

class Ticket(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.String(240), nullable=True)

    #Many to One
    user_id = db.Column(db.Integer,db.ForeignKey('user_id'), nullable=False)
    user = db.relationship('User', backref=db.backref('posts', lazy=True))

    #Many to One
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    project = db.relationship("Project", back_populates="tickets")

    priority = db.Column(db.Integer, nullable=True)
    status = db.Column(db.String(80), nullable=True)
    type = db.Column(db.String(80), nullable=True)
    created_date = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)
    updated_date = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)

    #One to many #TODO
    # comments =
    # attachments = 
    # changes =

#class comments
#class attachements
#class changes

    
