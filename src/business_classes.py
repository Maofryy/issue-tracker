
class Ticket():
    def __init__(self, title, desc, submitter, 
                project, users_list=[], priority=0, status=False, type="Default", created_date=0, 
                update_date=0, comment_list=[], attachements_list=[], changes_list=[]):
        self.title = title
        self.desc = desc 
        self.users_list = users_list
        self.submitter = submitter
        self.project =  project
        self.priority =  priority
        self.status =  status
        self.type =  type
        self.created_date =  created_date
        self.update_date = update_date
        self.comment_list =  comment_list
        self.attachements_list = attachements_list  
        self.changes_list = changes_list

    def __repr__(self):
        return "{} :  {}\n\t{} {}\n\t{} {}".format(
            self.title, self.desc, self.status, self.type,
             self.submitter, self.project)
    