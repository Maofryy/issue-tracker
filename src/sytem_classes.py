from pathlib import Path
import sqlite3
from src.handling_db import *

class CLI():
    def __init__(self, cmd, arg_list):
        self.cmd = cmd
        self.arg_list = arg_list
    
    def is_valid(self):
        pass
    
    def execute(self):
        pass

    def __repr__(self):
        return "{} -> {}".format(self.cmd, self.arg_list)

class TableCreator():
    def __init__(self, db_path):
        self.db_path = db_path
    
    def __create_tickets_table(self):
        # Enable foreign keys
        self.cursor.execute("PRAGMA foreign_keys = ON;")
        self.connection.commit()
        # Create tickets table 
        self.cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS tickets(
                ticket_id INTEGER PRIMARY KEY,
                ticket_title TEXT NOT NULL,
                ticket_desc TEXT NOT NULL,
                submitter TEXT NOT NULL,
                project_id INTEGER NOT NULL,
                ticket_users TEXT,
                ticket_priority INTEGER,
                ticket_status INTEGER,
                ticket_type INTEGER,
                ticket_created_date TEXT,
                ticket_update_date TEXT,
                ticket_comments TEXT,
                ticket_attachments TEXT,
                ticket_changes TEXT,
                FOREIGN KEY (project_id)
                    REFERENCES projects (project_id)
            )
            """
            )
        self.connection.commit()

    def __create_projects_table(self):
        # Enable foreign keys
        self.cursor.execute("PRAGMA foreign_keys = ON;")
        self.connection.commit()
        # Create projects table
        self.cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS projects(
                project_id INTEGER PRIMARY KEY,
                project_title TEXT NOT NULL,
                project_desc TEXT NOT NULL,
                project_users TEXT,
                project_tickets TEXT,
                project_created_date TEXT,
                project_update_date TEXT
            )
            """
        )
        self.connection.commit()

    def __create_users_table(self):
        # Enable foreign keys
        self.cursor.execute("PRAGMA foreign_keys = ON;")
        self.connection.commit()
        # Create users table
        self.cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS users(
                user_id INTEGER PRIMARY KEY,
                user_name TEXT NOT NULL,
                user_mail TEXT NOT NULL,
                user_role TEXT,
                user_tickets TEXT,
                user_projects TEXT
            )
            """
        )
        # Password in SQL ? 
        self.connection.commit()


    def create_tables(self):
        self.connection = sqlite3.connect(self.db_path)
        self.cursor = self.connection.cursor()

        self.__create_projects_table()
        self.connection.commit()

        self.__create_tickets_table()
        self.connection.commit()
        
        self.__create_users_table()
        self.connection.commit()

        self.connection.close()
        print("Tables created")
