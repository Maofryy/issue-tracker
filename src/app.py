from sqlalchemy import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
import sys

engine = create_engine('sqlite:///issueTracker.db', echo=False)

Base = automap_base()

# engine, suppose it has two tables 'user' and 'address' set up

# reflect the tables
Base.prepare(engine, reflect=True)
print(Base.classes.projects)
print(Base.classes.tickets)
print(Base.classes.users)


# mapped classes are now created with names by default
# matching that of the table name.
Users = Base.classes.users
Tickets = Base.classes.tickets

session = Session(engine)

# rudimentary relationships are produced
session.add(Tickets(
    ticket_id=1, 
    user_mail="foo@bar.com", submitter=Users(name="foo")))
session.commit()


# collection-based relationships are by default named
# "<classname>_collection"
u1 = session.query().all()
print (u1.address_collection)